const {
  Engine, 
  World, 
  Bodies, 
  Mouse,
  MouseConstraint,
  Constraint
} = Matter;

let ground;
const boxes = [];
//const boxes2 = [];
//const boxes1 = [];
let bird;
let world, engine;
let mConstraint;
let slingshot;

let birdImg;
let boxImg;
function preload(){
  birdImg = loadImage('bird1.png')
  boxImg = loadImage('boxf.png')
  backgroundImg = loadImage('backgroun1.jpg')
}

function setup() {
  const canvas = createCanvas(600, 400);
  
  engine = Engine.create();
  world = engine.world;
  
  ground = new Ground(width/2, height - 10, width, 20);
  for(let i = 0; i < 3; i++){
    boxes[i] = new Box(450, 300 - i*75, 79, 100);
  }
 /* for(let j = 0; j < 2; j++){
    boxes2[j] = new Box(420, 300 - j*75, 79, 100);
  }
  boxes1[0] = new Box(330, 300, 79, 100);*/
  bird = new Bird(200, 300, 30)
  
  slingshot = new SlingShot(150, 300, bird.body)
  
  const mouse = Mouse.create(canvas.elt)
  
  const options = {
    mouse : mouse
  }
  mConstraint = MouseConstraint.create(engine, options)
  World.add(world, mConstraint);
}

function keyPressed(){
  if(key == ' '){
    World.remove(world, bird.body);
    bird = new Bird(200, 300, 40);
    slingshot.attach(bird.body)
  }
}

function mouseReleased(){
  setTimeout(() => {
    slingshot.fly();
  }, 100)
}
function draw() {
  background(backgroundImg);
  Engine.update(engine);
  ground.show();
  for(let box of boxes){
    box.show();
  }
 /* for(let box of boxes2){
    box.show();
  }
  for(let box of boxes1){
    box.show();
  }*/
  slingshot.show();
  bird.show();
}